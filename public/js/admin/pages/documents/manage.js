var Documents = function () {
    
    return {

        initDoc: function (els) {
            $('#docTable').DataTable( {
                "ajax": "../../../../../canvas/media/documents/show",
                "columns": [
                    { "data": "filename" },
                    { "data": "url" },
                    {
                        sortable: false,
                        "render": function ( data, type, doc, meta ) {
                            // var content = '<a   href="#" class="btn " >Edit</a>'
                                        // + '<a onclick="notifyDelete('+image.id+')" data-toggle="modal" href="#alert" class="btn btn-danger deleteBtn" >Delete</a>'
                            var content = '<a onclick="notifyDelete('+doc.id+')" data-toggle="modal" href="#alert" class="btn btn-danger deleteBtn" >Delete</a>';
                            return content;
                        }
                    },
                ]
            } );
        }

    };
}();

$(document).ready(function() {

    
    window.notifyDelete = function(id){
        var _anchor = '<button type="button" class="btn default" data-dismiss="modal">Cancel</button>'
            + '<a type="button" class="btn blue" data-dismiss="modal" onclick="deleteImage('+id+')">OK</a>';
        $("#modal_footer").html(_anchor);
    }

    window.deleteImage = function(id){
        /* Send the data */
    $.ajax({
        url: "../../../../../../canvas/media/documents/delete&id="+id,
        async: false,
        type: "get",
        success: function (json) {
            toastr.success('Success!', 'Deleting Successful');
            $('alert').attr('data-dismiss','modal');
            $('#docTable').DataTable().ajax.reload();
        },
        error: function(json){
            toastr.error(json.result, json);
            $('alert').attr('data-dismiss','modal');
        }
    });
    }

} );// END Ducement Ready