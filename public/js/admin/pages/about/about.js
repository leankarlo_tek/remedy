var About = function () {

    var handleAboutForm = function() {

        $('.form_operations').validate({
            submitHandler: function(form, event) {
                event.preventDefault();

                for ( instance in CKEDITOR.instances ) {
                    CKEDITOR.instances[instance].updateElement();
                }

                var postData = $('.form_operations').serializeArray();
                var formURL = $('.form_operations').attr("action");
                $.ajax({
                    url : formURL,
                    type: "PUT",
                    data : postData,
                    success:function(data, textStatus, jqXHR) 
                    {
                        toastr.clear();
                        $('#saveloadscreen').modal('hide');
                        toastr.success('Succesfull',data.message);
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                        toastr.clear();
                        $('#saveloadscreen').modal('hide');
                        toastr.error('Failed',textStatus);
                    }
                });
                return false;
            }
        });


        $('.form_history').validate({
            submitHandler: function(form, event) {
                event.preventDefault();

                for ( instance in CKEDITOR.instances ) {
                    CKEDITOR.instances[instance].updateElement();
                }

                var postData = $('.form_history').serializeArray();
                var formURL = $('.form_history').attr("action");
                $.ajax({
                    url : formURL,
                    type: "PUT",
                    data : postData,
                    success:function(data, textStatus, jqXHR) 
                    {
                        toastr.clear();
                        $('#saveloadscreen').modal('hide');
                        toastr.success('Succesfull',data.message);
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                        toastr.clear();
                        $('#saveloadscreen').modal('hide');
                        toastr.error('Failed',textStatus);
                    }
                });
                return false;
            }
        });
    }
    
    return {

        init: function() {
            handleAboutForm();
        },

        initAbout: function (els) {



            var id = ReturnParam('id');
            /* Send the data */
            $.ajax({
                url:"../../../../../canvas/mainpage/operations",
                async: true,
                type: "get",
                success: function (json) {

                    CKEDITOR.replace( 'location', { toolbar: 'Basic', uiColor: '#9AB8F3' } );
                    CKEDITOR.replace( 'hours', { toolbar: 'Basic', uiColor: '#9AB8F3' } );
                    CKEDITOR.replace( 'reservation', { toolbar: 'Basic', uiColor: '#9AB8F3' } );
                    CKEDITOR.replace( 'storyContent', { toolbar: 'Basic', uiColor: '#9AB8F3' } );

                    $.each(json.data, function (idx, content) {
                        console.log(content);
                        switch(content.id) {
                            case '13':
                                CKEDITOR.instances.location.setData(content.content);
                                break;

                            case '14':
                                CKEDITOR.instances.hours.setData(content.content);
                                break;
                                
                            case '15':
                                CKEDITOR.instances.reservation.setData(content.content);
                                break;

                            case '1':
                                CKEDITOR.instances.storyContent.setData(content.content);
                                break;
                        }

                    });

                },
                error: function () {
                    console.log('error');
                }
            });
        },

    };
}();

$(document).ready(function() {

	window.ReturnParam = function(sParam){
        //Hakuna matata ^_^
        var sPageURL = window.location.href;
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    };

} );// END Ducement Ready