var Musics = function () {
    
    return {

        initMusics: function (els) {
            $('#musicTable').DataTable( {
                "ajax": "../../../../../canvas/media/musics/show",
                "columns": [
                    { "data": "filename" },
                    { "data": "url" },
                    {
                        sortable: false,
                        "render": function ( data, type, music, meta ) {
                            // var content = '<a   href="#" class="btn " >Edit</a>'
                                        // + '<a onclick="notifyDelete('+image.id+')" data-toggle="modal" href="#alert" class="btn btn-danger deleteBtn" >Delete</a>'
                            var content = '<a onclick="notifyDelete('+music.id+')" data-toggle="modal" href="#alert" class="btn btn-danger deleteBtn" >Delete</a>';
                            return content;
                        }
                    },
                ]
            } );
        }

    };
}();

$(document).ready(function() {

    
    window.notifyDelete = function(id){
        var _anchor = '<button type="button" class="btn default" data-dismiss="modal">Cancel</button>'
            + '<a type="button" class="btn blue" data-dismiss="modal" onclick="deleteImage('+id+')">OK</a>';
        $("#modal_footer").html(_anchor);
    }

    window.deleteImage = function(id){
        /* Send the data */
    $.ajax({
        url: "../../../../../../canvas/media/musics/delete&id="+id,
        async: false,
        type: "get",
        success: function (json) {
            toastr.success('Success!', 'Deleting Successful');
            $('alert').attr('data-dismiss','modal');
            $('#musicTable').DataTable().ajax.reload();
        },
        error: function(json){
            toastr.error(json.result, json);
            $('alert').attr('data-dismiss','modal');
        }
    });
    }

} );// END Ducement Ready