var musicFiles = function () {

    var handleMusicOnly = function() {
        Dropzone.options.mydropzone = {
            uploadMultiple: false,
            parallelUploads: 100,
            maxFilesize: 5,
            //addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            dictFileTooBig: 'File is bigger than 5MB',
            acceptedFiles: ".mp3",
            init:function() {
                this.on("success", function(file, response) {
                    file.previewElement.querySelector("span").textContent = response.filename;
                    toastr.success(response.message, 'Music Upload');
                });
            },
            error: function(file, response) {
                if($.type(response) === "string")
                    var message = response; //dropzone sends it's own error messages in string
                else
                    var message = response.message;

                file.previewElement.classList.add("dz-error");
                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i];
                    _results.push(node.textContent = message);
                }

                toastr.error('Whoops, something went wrong.', _results);

                return _results;
            }
        }       
    }
    return {

        init: function() {
            handleMusicOnly();
        }

    };
}();
