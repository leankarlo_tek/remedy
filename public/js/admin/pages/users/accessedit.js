$(document).ready(function() {

    window.GetAccessType = function(id)
    {
		$.ajax({
			url:"../../../../../canvas/users/accesstype/get&id="+id,
			async: true,
			type: "get",
			success: function (json) {
				$('#accesstype_id').val(json.data.id);
				$('#edit_accesstype_name').val(json.data.name);
			},
			error: function () {
				console.log('error');
			}
		});

        ClearModuleTable();

        var ModuleTable = $('#ModuleTable');
        ModuleTable.DataTable( {
            "ajax": "../../../../../canvas/users/accesstype/getuseraccessmodules&id="+id,            
            "columns": [
                {
                    "render": function ( data, type, data, meta ) {
                        var checkBox = '<input type="checkbox" class="checkboxes" value="'+data.id + '"';

                        var accessModule = data.access_module;

                        if (accessModule.length == 0)
                        {
                            checkBox = checkBox;
                        }   
                        else
                        {
                            checkBox = checkBox + ' checked="checked"';
                        }
                        return checkBox + '/>';
                    }      
                },
                { "data": "name" }
            ],
            'rowCallback': function(row, data, dataIndex){
               var accessModule = data.access_module;

               if (accessModule.length != 0)
               {
                   $(row).addClass('active');
               }

            }
        } );
        
        var tableWrapper = jQuery('#Table_wrapper');

        ModuleTable.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        ModuleTable.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); 

    }

    $('#ModuleTable tbody').on('click', 'input[type="checkbox"]', function(e){
        var $row = $(this).closest('tr');
        if(this.checked){
           $row.addClass('selected');
        } else {
           $row.removeClass('selected');
        }
   });

    window.ClearModuleTable = function()
    {
        var ModuleTable = $('#ModuleTable').DataTable();
        ModuleTable.destroy();
    }

    window.ReloadAccessTypeTable = function()
    {
        $('#AccessTypeTable').DataTable().ajax.reload();
    }

    window.Update = function(){
        $('#saveloadscreen').modal('show');
        var id = $('#accesstype_id').val();
        var name = $('#edit_accesstype_name').val();

        // VALIDATE INPUT
        if (name === '' || name == null)
        {
            $('#edit_accesstype_name').closest('.form-group').addClass('has-error');
            return;
        }
        else
        {
            $('#edit_accesstype_name').closest('.form-group').removeClass('has-error');
        }

        $.ajax({
            url:"../../../../../canvas/users/accesstype/update&id="+id + "&name="+name,
            async: true,
            type: "get",
            success: function (json) {
                if(json.result == 'true'){
                    UpdateAccessModuleDetails();
                    ReloadAccessTypeTable();
                    $('#saveloadscreen').modal('hide');
                    toastr.success(json.message, json.result);
                }
                else{
                    $('#saveloadscreen').modal('hide');
                    toastr.error(json.message, json.result);
                }
            },
            error: function () {
                console.log('error');
            }
        });
    }

    window.UpdateAccessModuleDetails = function(){
        var table = $('#ModuleTable').DataTable();
        var sData = table.rows('.active').data();

        var accesstypeid = $('#accesstype_id').val();

        // DELETE FIRST THE ACCESS MODULE RECORD
        DeleteAccessModuleByAccessType(accesstypeid)
        // INSERT EACH MODULE TO ACCESS MODULE TABLE
        $.each(sData, function(index, item) {
            var url = '../../../../../canvas/users/accesstype/createAccessModule&accessid='+accesstypeid+'&moduleid='+item.id
            $.ajax({
                url: url,
                async: false,
                type: "get",
                success: function (data) {
                    if(data.result == 'true'){
                        console.log(data.message);
                    }
                    else{
                        toastr.error(data.message, data.result);
                    }
                    
                },
                error: function () {
                    toastr.error('Something went wrong please contact ADMIN', 'ERROR');
                }
            });
        });
    }

    window.DeleteAccessModuleByAccessType = function(id){
        $.ajax({
            url: '../../../../../canvas/users/accesstype/deleteAccessModuleByAccessType&id='+id,
            async: false,
            type: "get",
            success: function (data) {
                if(data.result == 'true'){
                    console.log(data.message);
                }
                else{
                    toastr.error(data.message, data.result);
                }
                
            },
            error: function () {
                toastr.error('Something went wrong please contact ADMIN', 'ERROR');
            }
        });
    }

} );// END Ducement Ready