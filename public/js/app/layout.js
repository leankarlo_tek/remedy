var Layout = function () {
	
	// detect mobile device
	var isDay = function() {
		var hr = (new Date()).getHours();
		var daynightlink = document.getElementsById("stylesheet");
		if ( hr <= 18 ){
			daynightlink.href = "css/app/style-light.css";
		}
		else{
			daynightlink.href = "css/app/style.css";
		}
	}


	var navScroll = function() {
		console.log('scroll')
		var scroll_start = 0;
		var startchange = $('section');
		var offset = startchange.offset();
		$(document).scroll(function() { 
			scroll_start = $(this).scrollTop();
			if(scroll_start > offset.top) {
				$('.navbar').css('background-color', '#000');
			} else {
				$(".navbar").removeAttr("style");
			}
		});
	}

	var isMobileDevice = function() {
		return  ((
			navigator.userAgent.match(/Android/i) ||
			navigator.userAgent.match(/BlackBerry/i) ||
			navigator.userAgent.match(/iPhone|iPad|iPod/i) ||
			navigator.userAgent.match(/Opera Mini/i) ||
			navigator.userAgent.match(/IEMobile/i)
		) ? true : false);
	}

	// handle on page scroll
	var handleHeaderOnScroll = function() {
		if ($(window).scrollTop() > 60) {
			$("body").addClass("page-on-scroll");
		} else {
			$("body").removeClass("page-on-scroll");
		}
	}

	// handle go to top button
	var handleGo2Top = function () {       
		var Go2TopOperation = function(){
			var CurrentWindowPosition = $(window).scrollTop();// current vertical position
			if (CurrentWindowPosition > 300) {
				$(".go2top").show();
			} else {
				$(".go2top").hide();
			}
		};

		Go2TopOperation();// call headerFix() when the page was loaded
		if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
			$(window).bind("touchend touchcancel touchleave", function(e){
				Go2TopOperation();
			});
		} else {
			$(window).scroll(function() {
				Go2TopOperation();
			});
		}

		$(".go2top").click(function(e) {
			e.preventDefault();
			 $("html, body").animate({ scrollTop: 0 }, 600);
		});
	}

	var handleScrollspy = function() {
		var scrollspy = $('body').scrollspy({
			target: '.navbar-fixed-top',
			offset: 80
		});

		// fix active class on page load
		setTimeout(function() {
			$(window).scroll();
		}, 1000);       
	}

	var handleNavBarSubMenu = function() {
		$( ".sub-menu-header1" ).click(function() {

		  	if( $('.drowpdown-submenu1').css('display') == 'none' )
			{
				$('.drowpdown-submenu1').show();
			}
			else
			{
				$('.drowpdown-submenu1').hide();
			}

		});

		$( ".sub-menu-header2" ).click(function() {

		  	if( $('.drowpdown-submenu2').css('display') == 'none' )
			{
				$('.drowpdown-submenu2').show();
			}
			else
			{
				$('.drowpdown-submenu2').hide();
			}

		});
	}

	return {
		init: function () {
			//isDayorNight
			// isDay();
			navScroll();
			// handle go to top button
			handleGo2Top();

			// initial setup for fixed header
			handleHeaderOnScroll();

			// Handle one page scrollspy
			handleScrollspy();

			//handlenavbar submenu
			handleNavBarSubMenu();

			// handle minimized header on page scroll
			$(window).scroll(function() {
				handleHeaderOnScroll();
			});
		},

		// To get the correct viewport width based on  http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
		getViewPort: function() {
			var e = window,
				a = 'inner';
			if (!('innerWidth' in window)) {
				a = 'client';
				e = document.documentElement || document.body;
			}

			return {
				width: e[a + 'Width'],
				height: e[a + 'Height']
			};
		},
	};
}();

$(document).on('click', '.dropdown-menu', function (e) {
  e.stopPropagation();
});