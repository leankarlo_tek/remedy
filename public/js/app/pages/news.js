var News = function () {

    return {
        initCarousel: function () {
            console.log('test');
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:5,
                nav:true,
                items: 5,
                center: true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            });
        }
    };

}();