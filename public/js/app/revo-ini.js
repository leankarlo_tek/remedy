var RevosliderInit = function () {

    return {
        initRevoSlider: function () {
            var height = 580; // minimal height for medium resolution

            // smart height detection for all major screens
            if (Layout.getViewPort().width > 1600) {
                height = $(window).height() - $('.subscribe').outerHeight();  // full height for high resolution
            } else if (Layout.getViewPort().height > height) {
                height = Layout.getViewPort().height;
            }
            jQuery('.banner').revolution({
                delay: 1000,
                startwidth: 1170,
                startheight: 750,
                navigationArrows: "none",
                fullWidth: "on",
                fullScreen: "off",
                touchenabled:"off",                      // Enable Swipe Function : on/off
                onHoverStop: "off",                      // Stop Banner Timet at Hover on Slide on/off
                fullScreenOffsetContainer: "",
                parallax: {
        type: 'mouse+scroll',
        origo: 'slidercenter',
        speed: 400,
        levels: [5,10,15,20,25,30,35,40,
                 45,46,47,48,49,50,51,55],
        disable_onmobile: 'on'
    }
            });
        }
    };

}();