-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: projectcanvas
-- ------------------------------------------------------
-- Server version	5.6.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'icon-user','Users',0,1,'user_notifications',1,'users',1,'2015-11-23 18:34:59','2015-11-18 20:09:08'),(2,'fa fa-file-image-o','Media',0,0,NULL,2,'media',1,'2016-05-11 17:57:15','0000-00-00 00:00:00'),(3,'icon-notebook','Articles',1,1,'article_notifications',2,'articles',1,'2015-12-07 17:48:40','0000-00-00 00:00:00'),(4,'icon-note','Projects',0,0,NULL,1,'projects',1,'2015-12-13 15:24:57','0000-00-00 00:00:00'),(5,'icon-docs','Pages',0,0,NULL,1,'pages',1,'2016-02-23 07:04:22','0000-00-00 00:00:00'),(6,'fa fa-gears','Settings',0,0,NULL,0,'settings',1,'2015-12-13 17:00:58','0000-00-00 00:00:00'),(7,'fa fa-shopping-cart','E-Commerce',0,0,NULL,1,'products',1,'2016-02-24 02:42:28','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sub_modules`
--

LOCK TABLES `sub_modules` WRITE;
/*!40000 ALTER TABLE `sub_modules` DISABLE KEYS */;
INSERT INTO `sub_modules` VALUES (1,NULL,1,'User Management','1','2015-11-23 18:29:10','2015-11-18 20:09:08','management'),(2,NULL,1,'Create User','1','2015-11-23 18:29:10','0000-00-00 00:00:00','create'),(4,NULL,2,'Manage Media','1','2016-05-11 18:00:31','0000-00-00 00:00:00','manage'),(5,NULL,2,'Image Upload','2','2016-05-11 18:00:31','0000-00-00 00:00:00','upload'),(6,NULL,3,'New','2','2015-12-07 17:34:32','0000-00-00 00:00:00','new'),(7,NULL,3,'My Articles','2','2015-12-07 17:53:05','0000-00-00 00:00:00',' '),(8,NULL,3,'Management','1','2015-12-13 15:07:22','0000-00-00 00:00:00','manage'),(9,NULL,4,'New','1','2015-12-13 15:07:22','0000-00-00 00:00:00','new'),(10,NULL,4,'Manage','1','2015-12-13 15:07:53','0000-00-00 00:00:00','manage'),(11,NULL,5,'About Us','1','2015-12-13 16:29:26','0000-00-00 00:00:00','about'),(12,NULL,5,'Contact','1','2015-12-13 16:29:26','0000-00-00 00:00:00','contact'),(13,NULL,5,'Sliders','1','2016-02-23 15:02:25','0000-00-00 00:00:00','slider'),(14,NULL,5,'Promos','1','2015-12-13 16:37:32','0000-00-00 00:00:00','promos'),(15,NULL,6,'Module Management','0','2015-12-13 17:02:22','0000-00-00 00:00:00','modules'),(16,NULL,6,'General','0','2015-12-13 17:02:22','0000-00-00 00:00:00','general'),(17,NULL,7,'Products','2','2016-02-23 07:07:41','0000-00-00 00:00:00','manage'),(18,NULL,2,'File Upload',NULL,'2016-05-11 18:00:31','0000-00-00 00:00:00',NULL),(19,NULL,2,'Music Upload',NULL,'2016-05-11 18:01:14','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `sub_modules` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-12  2:07:26
