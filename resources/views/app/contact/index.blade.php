@extends('app.layout.main_layout')

@section('title')
<title>{{ Config::get('app.clientapp') }}</title>
@stop <!-- END title -->

@section('head')

@stop <!-- END head -->

@section('content')

<!-- BEGIN INTRO SECTION -->
    <section id="intro">
        <!-- Slider BEGIN -->
        <div class="page-slider">
            <div class="fullwidthbanner-container revolution-slider">
                <div class="banner">
                    <ul id="revolutionul">
                        <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="5000" >
                            <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                            <img src="../../uploads/images/aboutusbg.jpg" alt="">

                            <div class="caption tp-resizeme"
                                data-speed="5000"
                                data-start="1000"
                                data-easing="easeOutExpo"
                                data-x="left"
                                data-y="center" 
                                data-voffset="10"
                                data-hoffset="-55"
                                
                                >
                                <img src="../../uploads/images/logo-colored.png">
                            </div>

                            <div class="caption tp-resizeme"
                                data-speed="1000"
                                data-start="1000"
                                data-easing="easeinExpo"
                                data-voffset="-50"
                                data-x="right"
                                data-y="center"
                                >
                                <p class="subtitle-v1">Contact Us</p>
                                <p class="subtitle-v2">
                                    <br>Lorem ipsum dolor sit amet, et odio modus usu, eu 
                                    <br>habemus mentitum ius. 
                                    <br>Vide falli gloriatur ne sed. Ea consequat 
                                    <br>deseruisse eos, dolor timeam docendi usu ei. 
                                    <br>Ei integre suavitate ius. Doctus evertitur usu et.
                                    <br>Pri ad latine expetendis scriptorem. Ius eu cetero 
                                    <br>tritani omittam, mea dicam veniam partem ei, 
                                    <br>an vix tollit commodo suscipiantur. Eum quidam constituam 
                                    <br>honestatis ea. No usu zril nonumes 
                                    <br>legendos, saepe ridens similique at est. Ea mea novum atomorum, 
                                    <br>his ei vivendum facilisi posidonium. 
                                    <br>Reque vitae id usu, mei diam dolor inermis at, nam quis patrioque 
                                    <br>ei. Id qui tritani ceteros assentior.
                                </p>
                            </div>
                            
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <!-- Slider END -->
    </section>
    <!-- END INTRO SECTION -->

<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h1>Send in your inquiry, we will get back to you soonest</h1>
                </div>
                <div class="body" style="text-align: center;">
                    <form action="#" role="form">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="Email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="subject" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea rows="4" cols="50" class="form-control">
                            </textarea>
                        </div>
                        <button type="submit" class="btn"><i class="icon-ok"></i> Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection <!-- END content -->

@section('buttom_scripts')

<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app/revo-ini.js') }}" type="text/javascript"></script>

<script>
    jQuery(document).ready(function() {
        RevosliderInit.initRevoSlider();
    });
</script>


@stop <!-- END buttom_scripts -->