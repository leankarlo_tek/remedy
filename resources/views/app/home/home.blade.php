@extends('app.layout.main_layout')

@section('title')
    <title>{{ Config::get('app.clientapp') }}</title>
@stop <!-- END title -->

@section('head')

@stop <!-- END head -->

@section('content')

    <!-- BEGIN INTRO SECTION -->
    <section id="intro">
        <!-- Slider BEGIN -->
        <div class="page-slider">
            <div class="fullwidthbanner-container revolution-slider">
                <div class="banner">
                    <ul id="revolutionul">
                        <!-- THE NEW SLIDE -->
                        <li data-transition="slidehorizontal" data-slotamount="8" data-masterspeed="700" data-delay="5000" >
                            <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                            <img src="../../uploads/images/sliderbg1-2.jpg" alt="">

                            <div class="caption lfr tp-resizeme"
                                data-speed="5000"
                                data-start="1000"
                                data-x="left"
                                data-y="center" 
                                data-voffset="-50"
                                data-hoffset="-150"
                                
                                >
                                <img src="../../uploads/images/logo.png">
                            </div>

                            <div class="caption lfl tp-resizeme"
                                data-speed="2000"
                                data-start="2000"
                                data-voffset="-55"
                                data-x="right"
                                data-y="center"
                                >
                                <img src="../../uploads/images/logoname.png">
                            </div>
                            <div class="caption lfl tp-resizeme"
                                data-speed="5000"
                                data-start="2000"
                                data-voffset="50"
                                data-hoffset="-100"
                                data-x="right"
                                data-y="center"
                                >
                                 <button class="btn-clear">Click here</button>
                            </div>
                        </li>

                        <li data-transition="slidehorizontal" data-slotamount="7" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
                            <!-- MAIN IMAGE -->
                            <img src="../../uploads/images/sliderbg1-2.jpg"  alt="video_forest"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-fade fadeout fullscreenvideo"
                                data-x="0"
                                data-y="0"
                                data-speed="1000"
                                data-start="1100"
                                data-easing="Power4.easeOut"
                                data-endspeed="1500"
                                data-endeasing="Power4.easeIn"
                                data-autoplay="true"
                                data-autoplayonlyfirsttime="false"
                                data-nextslideatend="true"
                                data-forceCover="1"
                                data-dottedoverlay="twoxtwo"
                                data-aspectratio="16:9"
                                data-forcerewind="on"
                                style="z-index: 2">
                
                                <video class="video-js vjs-default-skin" preload="none" width="100%" height="100%" poster='images/video_forest.jpg' data-setup="{}">
                                <source src='../../../uploads/videos/bradley.mp4' type='video/mp4' />
                                <source src='../../../uploads/videos/bradley.webm' type='video/webm' />
                                <source src='../../../uploads/videos/bradley.ogv' type='video/ogg' />
                                </video>
                
                            </div>
                        </li>

                        <li data-transition="slidehorizontal" data-slotamount="7" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
                            <!-- MAIN IMAGE -->
                            <img src="../../uploads/images/sliderbg1-2.jpg"  alt="video_forest"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-fade fadeout fullscreenvideo"
                                data-x="0"
                                data-y="0"
                                data-speed="1000"
                                data-start="1100"
                                data-easing="Power4.easeOut"
                                data-endspeed="1500"
                                data-endeasing="Power4.easeIn"
                                data-autoplay="true"
                                data-autoplayonlyfirsttime="false"
                                data-nextslideatend="true"
                                data-forceCover="1"
                                data-dottedoverlay="twoxtwo"
                                data-aspectratio="16:9"
                                data-forcerewind="on"
                                style="z-index: 2">
                
                                <video class="video-js vjs-default-skin" preload="none" width="100%" height="100%" poster='images/video_forest.jpg' data-setup="{}">
                                <source src='../../../uploads/videos/thunderbird2.mp4' type='video/mp4' />
                                <source src='../../../uploads/videos/thunderbird2.webm' type='video/webm' />
                                <source src='../../../uploads/videos/thunderbird2.ogv' type='video/ogg' />
                                </video>
                
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <!-- Slider END -->
    </section>
    <!-- END INTRO SECTION -->

    <div class="page-content">
	<section id="header_bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hb_cont" >
						<h2>WHAT WE</h2>
						<h1 class="method2">OFFER</h1>
					</div>
				</div>
				<div class="col-md-7">
                    <div class="col-md-4">
                        <div class="hb_cont">
                            <a href="/service">
                                <div class="talent">
                                    
                                </div>
                            </a>
                        </div>
                    </div>
					
					<div class="col-md-4 border">
						<div class="hb_cont">
							<a href="/video">
                                <div class="event">
                                    
                                </div>
                            </a>
						</div>
					</div>

                    <div class="col-md-4">
                        <div class="hb_cont">
                            <a href="/event">
                                <div class="casting">
                                    
                                </div>
                            </a>
                        </div>
                    </div>
                    
				</div>
			</div>
		</div>
	</section>
	
	<section id="whoweare">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="wwa_cont_title">
						<h1>WHO WE ARE</h1>
					</div>
					<div class="wwa_cont_body">
						<p>
							Lorem ipsum dolor sit amet, et odio modus usu, eu habemus mentitum ius. 
							Vide falli gloriatur ne sed. Ea consequat deseruisse eos, dolor timeam docendi usu ei. 
							Ei integre suavitate ius. Doctus evertitur usu et.
						</p>
						<p>
							Pri ad latine expetendis scriptorem. Ius eu cetero tritani omittam, mea dicam veniam partem ei, 
							an vix tollit commodo suscipiantur. Eum quidam constituam honestatis ea. No usu zril nonumes 
							legendos, saepe ridens similique at est. Ea mea novum atomorum, his ei vivendum facilisi posidonium. 
							Reque vitae id usu, mei diam dolor inermis at, nam quis patrioque ei. Id qui tritani ceteros assentior.
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- BEGIN SLIDER -->
    <section id="news">
    	<div class="container">
            <h1>News and Event</h1>
    		<div class="row">
    			<div class="col-md-12">
                <div class="news-cont row">
    				<div class="imgdown-col">
                        <div class="row imgdownhead">
                            <h1>Lorem Ipsum</h1>
                            <p>Pri ad latine expetendis scriptorem. Ius eu cetero tritani omittam, mea dicam veniam partem ei, </p>
                        </div>
                        <a href="../../../news">
                            <div class="row imgdownimg" style="background: url('/uploads/images/news3.jpg');">
                            
                            </div>
                        </a>
                    </div>

                    <div class="imgdown-col">
                        <a href="../../../news">
                            <div class="row imgupimg" style="background: url('/uploads/images/news3.jpg');">
                            
                            </div>
                        </a>
                        <div class="row imguphead">
                            <h1>Lorem Ipsum</h1>
                            <p>Pri ad latine expetendis scriptorem. Ius eu cetero tritani omittam, mea dicam veniam partem ei, </p>
                        </div>
                    </div>

                    <div class="imgdown-col">
                        <div class="row imgdownhead">
                            <h1>Lorem Ipsum</h1>
                            <p>Pri ad latine expetendis scriptorem. Ius eu cetero tritani omittam, mea dicam veniam partem ei, </p>
                        </div>
                        <a href="../../../news">
                            <div class="row imgdownimg" style="background: url('/uploads/images/news3.jpg');">
                            
                            </div>
                        </a>
                    </div>
    			</div>
                </div>
    		</div>
    	</div>
    </section>
    <!-- END SLIDER -->

    <!-- BEGIN Meet Team -->
    <section id="meet-team">
    	<div class="container">
    		<div class="row header">
    			<div class="col-md-12">
    				<h1>Meet The Team</h1>
    			</div>
    		</div>
    		<div class="row body">
    			<div class="col-md-4">
    				<div class="profile-pic">
    					<a data-toggle="modal" href="#basic">
                  	  	  	<img src="../../images/team/team2.jpg" alt="">
                  	  	</a>
    				</div>
    				<div class="profile-desc col-md-6">
    					<h1>
    						Lorem Ipsum
    						</br>
    						<span>
    							Position
    						</span>
    					</h1>
    				</div>
    				<div class="profile-social col-md-6">
    					<a href="#"><i class="fa fa-facebook-square"></i></a>
    					<a href="#"><i class="fa fa-instagram"></i></a>
    				</div>
    			</div>
    			<div class="col-md-4">
    				<div class="profile-pic active">
    					<a data-toggle="modal" href="#basic">
                  	  	  	<img src="../../images/team/team2.jpg" alt="">
                  	  	</a>
    				</div>
    				<div class="profile-desc col-md-6">
    					<h1>
    						Lorem Ipsum
    						</br>
    						<span>
    							Position
    						</span>
    					</h1>
    				</div>
    				<div class="profile-social col-md-6">
    					<a href="#"><i class="fa fa-facebook-square"></i></a>
    					<a href="#"><i class="fa fa-instagram"></i></a>
    				</div>
    			</div>
    			<div class="col-md-4">
    				<div class="profile-pic">
    					<a data-toggle="modal" href="#basic">
                  	  	  	<img src="../../images/team/team2.jpg" alt="">
                  	  	</a>
    				</div>
    				<div class="profile-desc col-md-6">
    					<h1>
    					Lorem Ipsum
    					</br>
    					<span>
    						Position
    					</span>
    					</h1>
    				</div>
    				<div class="profile-social col-md-6">
    					<a href="#"><i class="fa fa-facebook-square"></i></a>
    					<a href="#"><i class="fa fa-instagram"></i></a>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    <!-- END Meet Team -->

    <section id="client">
    	<div class="container">
    		<div class="row">
    			<h1>
    				ClientTele
    			</h1>
    		</div>
    		<div class="row">
    			<div class="col-md-12 front-carousel">
                  	<div id="myCarousel" class="carousel slide">
                  	  <!-- Carousel items -->
                  	  	<div class="carousel-inner">
                  	  	  	<div class="item active">
                  	  	  	  	<img src="../../images/background/client-sample.jpg" alt="">
                  	  	  	</div>
                  	  	</div>
                  	  	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
   	 						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    						<span class="sr-only">Previous</span>
  						</a>
 						<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
 						  	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
 						  	<span class="sr-only">Next</span>
 						</a>
                  	</div>                
                </div>
    		</div>
    	</div>
    </section>


    <div class="modal bs-modal-lg fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                	<div>
    					<img src="../../images/team/team2.jpg" alt="">
    				</div>
    				<div>
    					<h1>Ms Caroline Hofer-Suarez</h1>
    					<h2>President | remedy Talent Management and Production Agency Inc.</h2>
    					<p>Lorem ipsum dolor sit amet, in possim vulputate qui, id odio lobortis nam. Eu nusquam deleniti scripserit duo, eam eu melius explicari complectitur, his utinam vituperata scriptorem et. No quo ullamcorper consequuntur, fugit nobis dolorem nam in. Id viderer feugiat placerat mei, ius eu meis invidunt, ex reque omnesque cum.</p>

						<p>Vix esse blandit sapientem id. Mei at malis scaevola, vis habeo copiosae prodesset no. Vis munere albucius gubergren ex, mei ea sensibus pericula, eu libris meliore nominati cum. Ut case tamquam usu, te omnesque hendrerit nam. Clita democritum mediocritatem vel et, pri at convenire maluisset voluptatibus. Amet lobortis te duo, vix solum voluptatum dissentiunt in.</p>

						<p>Sit ex expetendis ullamcorper consectetuer. Tale lorem cum no, in quod debitis reprimique has. Ex sea lobortis convenire. No iudico timeam usu.</p>
    				</div>
                </div>
            </div>
        </div>
    </div>

@endsection <!-- END content -->

@section('buttom_scripts')


	

@stop <!-- END buttom_scripts -->