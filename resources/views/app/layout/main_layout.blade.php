<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	@yield('title')
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="REMEDY EVENTS AND TALENT MANAGEMENT" name="description"/>
	<meta content="teknolohiya.ph" name="author"/>
	<link href="https://fonts.googleapis.com/css?family=Crimson+Text|Quicksand|Raleway:100,400" rel="stylesheet">
	<link href="{{ asset('packages/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />

	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="{{ asset('packages/slider-revolution-slider/rs-plugin/css/settings.css') }}" rel="stylesheet">
	<!-- END PAGE LEVEL PLUGIN STYLES -->

	{{-- <link id="stylesheet" href="css/app/style.css" rel="stylesheet" type="text/css"/> --}}
	<link id="stylesheet" href="{{ asset('css/app/style.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('css/app/layout.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('css/app/media.css') }}" rel="stylesheet" type="text/css"/>

	@yield('head')

</head>

<body>
	<!-- BEGIN HEADER -->

	{{-- Begin Nav Bar --}}


	<nav class="navbar navbar-fixed-top">
		<div class="container">
			<div class="navbar-header" style="color: #fff !important">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><img src="/images/logos/logo_top.png" alt="Dispute Bills">
				</a>
			</div>
			<div id="navbar3" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="/">Home</a></li>
					<li><a href="/about">About</a></li>
					<li class="dropdown">
						<a href="#" class="" data-toggle="dropdown">Services</a>
						<ul class="dropdown-menu" style="width: 350px !important;">
							<li >
								<a href="/service">
									Entertainment
								</a>
								{{-- <ul class="drowpdown-submenu1">
									<li><a href="/service?page=model">Models</a></li>
									<li><a href="/service?page=musicians">Musicians</a></li>
									<li><a href="/service?page=host">Host</a></li>
									<li><a href="/service?page=dancers">Dancers</a></li>
									<li><a href="/service?page=special Acts">Special Acts</a></li>
									<li><a href="/service?page=Ddj">DJs</a></li>
								</ul> --}}
							</li>
							<li >
								<a href="/video">
									Video Production
								</a>
								{{-- <ul class="drowpdown-submenu1">
									<li><a href="/service?page=model">Models</a></li>
									<li><a href="/service?page=musicians">Musicians</a></li>
									<li><a href="/service?page=host">Host</a></li>
									<li><a href="/service?page=dancers">Dancers</a></li>
									<li><a href="/service?page=special Acts">Special Acts</a></li>
									<li><a href="/service?page=Ddj">DJs</a></li>
								</ul> --}}
							</li>
							<li>
								<a href="/event">
									Event Production
								</a>
								{{-- <ul class="drowpdown-submenu2">
									<li><a href="/event?page=Conceptualize">Conceptualize</a></li>
									<li><a href="/event?page=production">Production Team</a></li>
									<li><a href="/event?page=creatives">Creatives</a></li>
									<li><a href="/event?page=technical">Technical Equipment and Logistics</a></li>
									<li><a href="/event?page=documents">Documentation</a></li>
									<li><a href="/event?page=casting">Casting</a></li>
								</ul> --}}
							</li>
						</ul>
					</li>
					<li><a href="/news">News and Events</a></li>
					<li style="border-right: 0px solid !important;"><a href="/contact">Contact</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		<!--/.container-fluid -->
	</nav>

	<!-- Header END -->

	
	@yield('content')
	

	<section id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4 menu">
					<h1>INFORMATION</h1>
					<ul>
						<li>
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">About</a>
						</li>
						<li>
							<a href="#">Services</a>
						</li>
						<li>
							<a href="#">News and Events</a>
						</li>
						<li>
							<a href="#">Contact</a>
						</li>
					</ul>
				</div>
				<div class="col-md-8 social">
					<h1>STAY CONNECTED</h1>
					<ul>
						<li>
							<a href="#"><i class="fa fa-facebook-square"></i></a>
    						<a href="#"><i class="fa fa-instagram"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<script src="{{ asset('packages/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/jquery-migrate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/app/revo-ini.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/app/layout.js') }}" type="text/javascript"></script>
	<script>
		jQuery(document).ready(function() {
			RevosliderInit.initRevoSlider();
		});
	</script>
	<!-- END Cubeportfolio -->
	
	
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {    
			Layout.init();
		});
	</script>
	@yield('buttom_scripts')

</body>

</html>