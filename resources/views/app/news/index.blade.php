@extends('app.layout.main_layout')

@section('title')
    <title>{{ Config::get('app.clientapp') }}</title>
@stop <!-- END title -->

@section('head')
    <link href="{{ asset('packages/carousel-owl-carousel/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('packages/carousel-owl-carousel/owl-carousel/owl.theme.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('packages/carousel-owl-carousel/owl-carousel/owl.transitions.css') }}" rel="stylesheet" type="text/css"/>
@stop <!-- END head -->

@section('content')

    <!-- BEGIN INTRO SECTION -->
    <section id="intro" style="background: #f1f3f2 !important;">
        <div class="page-slider">
            <div class="fullwidthbanner-container revolution-slider">
                <div class="banner">
                    <ul id="revolutionul">
                        <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="5000" >
                            <img src="../../uploads/images/lights-festival-party-dancing-bw.jpg" alt="">
                            <div class="caption tp-resizeme"
                                data-speed="5000"
                                data-start="1000"
                                data-easing="easeOutExpo"
                                data-x="center"
                                data-y="center" 
                                >
                                <p class="subtitle-v3">NEWS AND EVENTS</p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- END INTRO SECTION -->

    <section id="pastevents">
        {{-- <div class="container"> --}}
            <div class="row">
                <h1>PAST EVENTS</h1>
            </div>
            <div class="row">
                <div class="news-cont row odd">
                    <div class="col-md-4">
                        <img src="../../uploads/images/news3.jpg">
                    </div>
                    <div class="col-md-8">
                        <h1>Lorem Ipsum | October 15, 2016</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        </p>
                        <p><a href="#basic" data-toggle="modal" >read more..</a></p>
                    </div>
                </div>
                <div class="news-cont row even">
                    <div class="col-md-8">
                        <h1>Lorem Ipsum | October 15, 2016</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        </p>
                         <p><a href="#basic" data-toggle="modal" >read more..</a></p>
                    </div>
                    <div class="col-md-4">
                        <img src="../../uploads/images/news3.jpg">
                    </div>
                </div>
                <div class="news-cont row odd">
                    <div class="col-md-4">
                        <img src="../../uploads/images/news3.jpg">
                    </div>
                    <div class="col-md-8">
                        <h1>Lorem Ipsum | October 15, 2016</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                        </p>
                        <p><a href="#basic" data-toggle="modal" >read more..</a></p>
                    </div>
                </div>
            </div>
            
        {{-- </div> --}}
    </section>

    {{-- upcoming events --}}
    <section id="upcoming">
        <div class="container">
            <div class="row">
                <h1>UPCOMING EVENTS</h1>
            </div>
            <div class="row title">
                <h1>April 2017</h1>
            </div>
            <div class="calendar">

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>


            </div>

            <div class="row title">
                <h1>May 2017</h1>
            </div>
            <div class="calendar">

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>

                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>
                <div class="col-md-2 date">
                    <h1>2 Friday</h1>
                </div>
                <div class="col-md-4 info">
                    <p>- Pre Pageant, Ms. City of Dreams <br>
                    <strong>City of Dreams</strong></p>
                </div>


            </div>

        </div>
    </section>
    {{-- end upcoming events --}}

    {{-- modals --}}
    <div class="modal bs-modal-lg fade news-click" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div class="row ">
                        <img src="../../uploads/images/lights-party-dancing-music.jpg">
                    </div>
                    <div class="row">
                        <div class="event-carousel">
                            <div class="owl-carousel owl-theme">
                                <div class="item">
                                    <img src="../../uploads/images/colored1.jpg">
                                </div>
                                <div class="item">
                                    <img src="../../uploads/images/colored2.jpg">
                                </div>
                                <div class="item">
                                    <img src="../../uploads/images/colored3.jpg">
                                </div>
                                <div class="item">
                                    <img src="../../uploads/images/colored4.jpg">
                                </div>
                                <div class="item">
                                    <img src="../../uploads/images/colored5.jpg">
                                </div>
                                <div class="item">
                                    <img src="../../uploads/images/news1.jpg">
                                </div>
                                <div class="item">
                                    <img src="../../uploads/images/news2.jpg">
                                </div>
                                <div class="item">
                                    <img src="../../uploads/images/news3.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <p>TEST Lorem ipsum dolor sit amet, duo te solum perpetua abhorreant, sed an nonumes perpetua qualisque. Consequat vituperatoribus eu per, nec no wisi eruditi utroque. Et oblique ullamcorper usu, has probo inani ex. Ne eam falli putant partiendo, ridens scaevola in quo.

                        <br>Nam etiam paulo viderer te. Ne cum graeci saperet prodesset. Mel te paulo vituperatoribus. Maiorum ponderum mandamus no per, melius malorum quo id, eu mea sumo adipisci senserit. Ex pro hinc neglegentur, sea te libris mediocrem facilisis.
                        <br>
                        <br>Ea vel eripuit cotidieque, ius ea eirmod impedit voluptaria, te quo laudem nostrud impedit. Insolens qualisque ne nam, nec in tation fuisset oporteat, probo quaestio expetenda eu qui. Quem tale ridens eam an, nam eu mutat illum. Ne prima eligendi nec, case ullum doctus id pri, id solum mediocritatem pri. Eum ipsum mundi at. Ei mei enim facilis, affert ancillae ut his. In impedit tibique corpora quo.
                        <br>
                        <br>Et nec autem accusamus interesset, eam paulo deserunt lobortis at. Quas virtute deterruisset usu in, omittantur concludaturque mei eu. Est ei legere molestie intellegebat, ei quaestio gubergren cum, te animal epicurei imperdiet vim. In diam malorum pri, graecis recusabo eos ut. Mei eu malis equidem singulis, per maiestatis scripserit in.
                        <br>
                        <br>Sit stet populo no, no mel exerci cetero conceptam, ei qui diam moderatius. Vix atqui minimum ocurreret et, mei et ridens vidisse consectetuer. Nam at labores nominati temporibus. Velit quando per ad, amet impetus tamquam ne ius, eu phaedrum corrumpit argumentum ius. Eius zril nam at, modo convenire scriptorem te nec.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end modals --}}

@endsection <!-- END content -->

@section('buttom_scripts')

<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/carousel-owl-carousel/owl-carousel/owl.carousel.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app/revo-ini.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app/pages/news.js') }}" type="text/javascript"></script>

<script>
    jQuery(document).ready(function() {
        RevosliderInit.initRevoSlider();
        News.initCarousel();
    });
</script>
    

@stop <!-- END buttom_scripts -->