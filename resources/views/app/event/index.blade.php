@extends('app.layout.main_layout')

@section('title')
    <title>{{ Config::get('app.clientapp') }}</title>
@stop <!-- END title -->

@section('head')

@stop <!-- END head -->

@section('content')

    <!-- BEGIN INTRO SECTION -->
    <section id="intro">
        <!-- Slider BEGIN -->
        <div class="page-slider">
            <div class="fullwidthbanner-container revolution-slider">
                <div class="banner">
                    <ul id="revolutionul">
                        <li data-transition="slidehorizontal" data-slotamount="7" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
                            <!-- MAIN IMAGE -->
                            <img src="../../uploads/images/sliderbg1-2.jpg"  alt="video_forest"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-fade fadeout fullscreenvideo"
                                data-x="0"
                                data-y="0"
                                data-speed="1000"
                                data-start="1100"
                                data-easing="Power4.easeOut"
                                data-endspeed="1500"
                                data-endeasing="Power4.easeIn"
                                data-autoplay="true"
                                data-autoplayonlyfirsttime="false"
                                data-nextslideatend="true"
                                data-forceCover="1"
                                data-dottedoverlay="twoxtwo"
                                data-aspectratio="16:9"
                                data-forcerewind="on"
                                style="z-index: 2">
                
                                <video class="video-js vjs-default-skin" preload="none" width="100%" height="100%" poster='images/video_forest.jpg' data-setup="{}">
                                <source src='../../../uploads/videos/adp.mp4' type='video/mp4' />
                                <source src='../../../uploads/videos/adp.webm' type='video/webm' />
                                <source src='../../../uploads/videos/adp.ogv' type='video/ogg' />
                                </video>
                
                            </div>
                        </li>
                        <li data-transition="slidehorizontal" data-slotamount="7" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
                            <!-- MAIN IMAGE -->
                            <img src="../../uploads/images/sliderbg1-2.jpg"  alt="video_forest"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-fade fadeout fullscreenvideo"
                                data-x="0"
                                data-y="0"
                                data-speed="1000"
                                data-start="1100"
                                data-easing="Power4.easeOut"
                                data-endspeed="1500"
                                data-endeasing="Power4.easeIn"
                                data-autoplay="true"
                                data-autoplayonlyfirsttime="false"
                                data-nextslideatend="true"
                                data-forceCover="1"
                                data-dottedoverlay="twoxtwo"
                                data-aspectratio="16:9"
                                data-forcerewind="on"
                                style="z-index: 2">
                
                                <video class="video-js vjs-default-skin" preload="none" width="100%" height="100%" poster='images/video_forest.jpg' data-setup="{}">
                                <source src='../../../uploads/videos/SPICRM.mp4' type='video/mp4' />
                                <source src='../../../uploads/videos/SPICRM.webm' type='video/webm' />
                                <source src='../../../uploads/videos/SPICRM.ogv' type='video/ogg' />
                                </video>
                
                            </div>
                        </li>

                        <li data-transition="slidehorizontal" data-slotamount="7" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
                            <!-- MAIN IMAGE -->
                            <img src="../../uploads/images/sliderbg1-2.jpg"  alt="video_forest"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-fade fadeout fullscreenvideo"
                                data-x="0"
                                data-y="0"
                                data-speed="1000"
                                data-start="1100"
                                data-easing="Power4.easeOut"
                                data-endspeed="1500"
                                data-endeasing="Power4.easeIn"
                                data-autoplay="true"
                                data-autoplayonlyfirsttime="false"
                                data-nextslideatend="true"
                                data-forceCover="1"
                                data-dottedoverlay="twoxtwo"
                                data-aspectratio="16:9"
                                data-forcerewind="on"
                                style="z-index: 2">
                
                                <video class="video-js vjs-default-skin" preload="none" width="100%" height="100%" poster='images/video_forest.jpg' data-setup="{}">
                                <source src='../../../uploads/videos/thunderbird2.mp4' type='video/mp4' />
                                <source src='../../../uploads/videos/thunderbird2.webm' type='video/webm' />
                                <source src='../../../uploads/videos/thunderbird2.ogv' type='video/ogg' />
                                </video>
                
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- Slider END -->
    </section>
    <!-- END INTRO SECTION -->
    
    <section id="servicebody">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h1>Event Production</h1>
                    </div>
                    <div class="body">
                        <p>
                            Lorem ipsum dolor sit amet, et odio modus usu, eu habemus mentitum ius. 
                            Vide falli gloriatur ne sed. Ea consequat deseruisse eos, dolor timeam docendi usu ei. 
                            Ei integre suavitate ius. Doctus evertitur usu et.
                        </p>
                        <p>
                            Pri ad latine expetendis scriptorem. Ius eu cetero tritani omittam, mea dicam veniam partem ei, 
                            an vix tollit commodo suscipiantur. Eum quidam constituam honestatis ea. No usu zril nonumes 
                            legendos, saepe ridens similique at est. Ea mea novum atomorum, his ei vivendum facilisi posidonium. 
                            Reque vitae id usu, mei diam dolor inermis at, nam quis patrioque ei. Id qui tritani ceteros assentior.

                            Pri ad latine expetendis scriptorem. Ius eu cetero tritani omittam, mea dicam veniam partem ei, 
                            an vix tollit commodo suscipiantur. Eum quidam constituam honestatis ea. No usu zril nonumes 
                            legendos, saepe ridens similique at est. Ea mea novum atomorum, his ei vivendum facilisi posidonium. 
                            Reque vitae id usu, mei diam dolor inermis at, nam quis patrioque ei. Id qui tritani ceteros assentior.

                            Pri ad latine expetendis scriptorem. Ius eu cetero tritani omittam, mea dicam veniam partem ei, 
                            an vix tollit commodo suscipiantur. Eum quidam constituam honestatis ea. No usu zril nonumes 
                            legendos, saepe ridens similique at est. Ea mea novum atomorum, his ei vivendum facilisi posidonium. 
                            Reque vitae id usu, mei diam dolor inermis at, nam quis patrioque ei. Id qui tritani ceteros assentior.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="events">
        {{-- <div class="container"> --}}
            <div class="row">
                <div class="col-md-4">
                    <a data-toggle="modal" href="#basic">
                        <img src="../../uploads/images/news3.jpg">
                        <h1>Concept Development</h1>
                    </a>
                </div>
                <div class="col-md-4">
                    <a data-toggle="modal" href="#basic">
                        <img src="../../uploads/images/colored1.jpg">
                        <h1>Production Team</h1>
                    </a>
                </div>
                <div class="col-md-4">
                    <a data-toggle="modal" href="#basic">
                        <img src="../../uploads/images/news2.jpg">
                        <h1>Creatives</h1>
                    </a>
                </div>
                <div class="col-md-4">
                    <a data-toggle="modal" href="#basic">
                        <img src="../../uploads/images/colored3.jpg">
                        <h1>Production Design and Staging</h1>
                    </a>
                </div>
                <div class="col-md-4">
                    <a data-toggle="modal" href="#basic">
                        <img src="../../uploads/images/colored4.jpg">
                        <h1>Technical Equipment and Logistics</h1>
                    </a>
                </div>
                <div class="col-md-4">
                    <a data-toggle="modal" href="#basic">
                        <img src="../../uploads/images/colored5.jpg">
                        <h1>Documentation</h1>
                    </a>
                </div>
            </div>
        {{-- </div> --}}
    </section>

    <div class="modal bs-modal-lg fade eventsModal" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="header">
                                <h1>Technical Equipment and Logistics</h1>
                            <p>Lorem ipsum dolor sit amet, in possim vulputate qui, id odio lobortis nam. Eu nusquam deleniti scripserit duo, eam eu melius explicari complectitur, his utinam vituperata scriptorem et. No quo ullamcorper consequuntur, fugit nobis dolorem nam in. Id viderer feugiat placerat mei, ius eu meis invidunt, ex reque omnesque cum.</p>
                            <ul>
                                <li>
                                    <h1>Lights & Sound System</h1>
                                    <p>Lorem ipsum dolor sit amet, in possim vulputate qui, id odio lobortis nam. Eu nusquam deleniti scripserit duo, eam eu melius explicari complectitur, his utinam vituperata scriptorem et. No quo ullamcorper consequuntur, fugit nobis dolorem nam in. Id viderer feugiat placerat mei, ius eu meis invidunt, ex reque omnesque cum.</p>
                                </li>
                                <li>
                                    <h1>Audio and Visual Equipment</h1>
                                    <p>Lorem ipsum dolor sit amet, in possim vulputate qui, id odio lobortis nam. Eu nusquam deleniti scripserit duo, eam eu melius explicari complectitur, his utinam vituperata scriptorem et. No quo ullamcorper consequuntur, fugit nobis dolorem nam in. Id viderer feugiat placerat mei, ius eu meis invidunt, ex reque omnesque cum.</p>
                                </li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <ul class="img-col">
                                <li>
                                    <img src="../../uploads/images/colored1.jpg">
                                </li>
                                <li>
                                    <img src="../../uploads/images/colored2.jpg">
                                </li>
                                <li>
                                    <img src="../../uploads/images/colored3.jpg">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection <!-- END content -->

@section('buttom_scripts')

<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app/revo-ini.js') }}" type="text/javascript"></script>

<script>
    jQuery(document).ready(function() {
        RevosliderInit.initRevoSlider();
    });
</script>
    

@stop <!-- END buttom_scripts -->