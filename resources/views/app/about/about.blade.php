@extends('app.layout.main_layout')

@section('title')
    <title>{{ Config::get('app.clientapp') }}</title>
@stop <!-- END title -->

@section('head')

@stop <!-- END head -->

@section('content')

    <!-- BEGIN INTRO SECTION -->
    <section id="aboutslider">
        <!-- Slider BEGIN -->
        <div class="page-slider">
            <div class="fullwidthbanner-container revolution-slider">
                <div class="banner">
                    <ul id="revolutionul">
                        <!-- THE NEW SLIDE -->
                        <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="5000" >
                            <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                            <img src="../../uploads/images/aboutusbg.jpg" alt="">

                            <div class="caption tp-resizeme"
                                data-speed="5000"
                                data-start="1000"
                                data-easing="easeOutExpo"
                                data-x="left"
                                data-y="center" 
                                data-voffset="10"
                                data-hoffset="-55"
                                
                                >
                                <img src="../../uploads/images/logo-colored.png">
                            </div>

                            <div class="caption tp-resizeme"
                                data-speed="1000"
                                data-start="1000"
                                data-easing="easeinExpo"
                                data-voffset="-50"
                                data-x="right"
                                data-y="center"
                                >
                                <p class="subtitle-v1">Who we are</p>
                                <p class="subtitle-v2">
                                    <br>Lorem ipsum dolor sit amet, et odio modus usu, eu 
                                    <br>habemus mentitum ius. 
                                    <br>Vide falli gloriatur ne sed. Ea consequat 
                                    <br>deseruisse eos, dolor timeam docendi usu ei. 
                                    <br>Ei integre suavitate ius. Doctus evertitur usu et.
                                    <br>Pri ad latine expetendis scriptorem. Ius eu cetero 
                                    <br>tritani omittam, mea dicam veniam partem ei, 
                                    <br>an vix tollit commodo suscipiantur. Eum quidam constituam 
                                    <br>honestatis ea. No usu zril nonumes 
                                    <br>legendos, saepe ridens similique at est. Ea mea novum atomorum, 
                                    <br>his ei vivendum facilisi posidonium. 
                                    <br>Reque vitae id usu, mei diam dolor inermis at, nam quis patrioque 
                                    <br>ei. Id qui tritani ceteros assentior.
                                </p>
                            </div>
                            
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <!-- Slider END -->
    </section>
    <!-- END INTRO SECTION -->

    <!-- BEGIN Meet Team -->
    <section id="meet-team2">
    	<div class="container">
    		<div class="row header">
    			<div class="col-md-12">
    				<h1>Meet The Team</h1>
    			</div>
    		</div>
    		<div class="row body">
    			<div class="col-md-4">
    				<div class="profile-pic">
    					<a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
    				</div>
    				<div class="profile-desc col-md-6">
    					<h1>
    						Lorem Ipsum
    						</br>
    						<span>
    							Position
    						</span>
    					</h1>
    				</div>
    				<div class="profile-social col-md-6">
    					<a href="#"><i class="fa fa-facebook-square"></i></a>
    					<a href="#"><i class="fa fa-instagram"></i></a>
    				</div>
    			</div>
    			<div class="col-md-4">
    				<div class="profile-pic ">
    					<a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
    				</div>
    				<div class="profile-desc col-md-6">
    					<h1>
    						Lorem Ipsum
    						</br>
    						<span>
    							Position
    						</span>
    					</h1>
    				</div>
    				<div class="profile-social col-md-6">
    					<a href="#"><i class="fa fa-facebook-square"></i></a>
    					<a href="#"><i class="fa fa-instagram"></i></a>
    				</div>
    			</div>
    			<div class="col-md-4">
    				<div class="profile-pic">
    					<a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
    				</div>
    				<div class="profile-desc col-md-6">
    					<h1>
    					Lorem Ipsum
    					</br>
    					<span>
    						Position
    					</span>
    					</h1>
    				</div>
    				<div class="profile-social col-md-6">
    					<a href="#"><i class="fa fa-facebook-square"></i></a>
    					<a href="#"><i class="fa fa-instagram"></i></a>
    				</div>
    			</div>
    		</div>
            <div class="row body">
                <div class="col-md-4">
                    <div class="profile-pic">
                        <a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
                    </div>
                    <div class="profile-desc col-md-6">
                        <h1>
                            Lorem Ipsum
                            </br>
                            <span>
                                Position
                            </span>
                        </h1>
                    </div>
                    <div class="profile-social col-md-6">
                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-pic">
                        <a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
                    </div>
                    <div class="profile-desc col-md-6">
                        <h1>
                            Lorem Ipsum
                            </br>
                            <span>
                                Position
                            </span>
                        </h1>
                    </div>
                    <div class="profile-social col-md-6">
                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-pic">
                        <a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
                    </div>
                    <div class="profile-desc col-md-6">
                        <h1>
                        Lorem Ipsum
                        </br>
                        <span>
                            Position
                        </span>
                        </h1>
                    </div>
                    <div class="profile-social col-md-6">
                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="row body">
                <div class="col-md-4">
                    <div class="profile-pic">
                        <a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
                    </div>
                    <div class="profile-desc col-md-6">
                        <h1>
                            Lorem Ipsum
                            </br>
                            <span>
                                Position
                            </span>
                        </h1>
                    </div>
                    <div class="profile-social col-md-6">
                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-pic">
                        <a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
                    </div>
                    <div class="profile-desc col-md-6">
                        <h1>
                            Lorem Ipsum
                            </br>
                            <span>
                                Position
                            </span>
                        </h1>
                    </div>
                    <div class="profile-social col-md-6">
                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-pic">
                        <a data-toggle="modal" href="#basic">
                            <img src="../../images/team/team2.jpg" alt="">
                        </a>
                    </div>
                    <div class="profile-desc col-md-6">
                        <h1>
                        Lorem Ipsum
                        </br>
                        <span>
                            Position
                        </span>
                        </h1>
                    </div>
                    <div class="profile-social col-md-6">
                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
    	</div>
    </section>
    <!-- END Meet Team -->

    <section id="client">
        <div class="container">
            <div class="row">
                <h1>
                    ClientTele
                </h1>
            </div>
            <div class="row">
                <div class="col-md-12 front-carousel">
                    <div id="myCarousel" class="carousel slide">
                      <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="../../images/background/client-sample.jpg" alt="">
                            </div>
                        </div>
                        <!-- Carousel nav -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>                
                </div>
            </div>
        </div>
    </section>

    <div class="modal bs-modal-lg fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div>
                        <img src="../../images/team/team2.jpg" alt="">
                    </div>
                    <div>
                        <h1>Ms Caroline Hofer-Suarez</h1>
                        <h2>President | remedy Talent Management and Production Agency Inc.</h2>
                        <p>Lorem ipsum dolor sit amet, in possim vulputate qui, id odio lobortis nam. Eu nusquam deleniti scripserit duo, eam eu melius explicari complectitur, his utinam vituperata scriptorem et. No quo ullamcorper consequuntur, fugit nobis dolorem nam in. Id viderer feugiat placerat mei, ius eu meis invidunt, ex reque omnesque cum.</p>

                        <p>Vix esse blandit sapientem id. Mei at malis scaevola, vis habeo copiosae prodesset no. Vis munere albucius gubergren ex, mei ea sensibus pericula, eu libris meliore nominati cum. Ut case tamquam usu, te omnesque hendrerit nam. Clita democritum mediocritatem vel et, pri at convenire maluisset voluptatibus. Amet lobortis te duo, vix solum voluptatum dissentiunt in.</p>

                        <p>Sit ex expetendis ullamcorper consectetuer. Tale lorem cum no, in quod debitis reprimique has. Ex sea lobortis convenire. No iudico timeam usu.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection <!-- END content -->

@section('buttom_scripts')

<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('packages/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app/revo-ini.js') }}" type="text/javascript"></script>


<script>
	jQuery(document).ready(function() {
		RevosliderInit.initRevoSlider();
	});
</script>
	

@stop <!-- END buttom_scripts -->