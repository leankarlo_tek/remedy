@extends('canvas.layouts.dashboard.mainlayout')

@section('title')
	<title>{{ Config::get('app.name') }} | Upload Document</title>
@stop

@section('head')
	<!-- BEGIN PAGE STYLES -->
	<link rel="stylesheet" type="text/css" href="{{ asset('packages/dropzone/css/dropzone.css')}}"/>
	{{-- <link href="{{ URL::to('stylesheets/admin/pages/newsfeed/newsfeed.css') }}" rel="stylesheet" type="text/css"/> --}}
	<!-- END PAGE STYLES -->
@stop


@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-12">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file-picture-o"></i>Document Form
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<p>
									<span class="label label-danger">
									NOTE: </span>
									&nbsp; Kindly use recomended browsers Latest Chrome, Firefox, Safari, Opera & Internet 	Explorer 10. <br /><br />
									Accepted File Types: word file, pdf file, google file and zip/rar files only
								</p>
								{!! Form::open(array('action' => 'Canvas\DocumentController@uploadDocuments', 'enctype' => 'multipart/form-data', 'class'=>'dropzone', 'id'=>'mydropzone','files'=>'true')) !!}
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>

@endsection

@section('buttom_scripts')

	<!-- BEGIN PAGE SCRIPTS -->
	<script type="text/javascript" src="{{ asset('packages/dropzone/dropzone.js')}}"></script>

	<script type="text/javascript" src="{{ asset('js/admin/pages/documents/upload.js')}}"></script>
	<script>
	jQuery(document).ready(function() {
	   documentFiles.init();
	});
	</script>

	<!-- END PAGE SCRIPTS -->

@stop