@extends('canvas.layouts.login.index')

@section('title')
	<title>{{ Config::get('app.name') }} - Login</title>
@stop

@section('head')
	<!-- BEGIN PAGE STYLES -->
	<link href="{{ asset('css/admin/pages/login/login.css') }}" rel="stylesheet" type="text/css"/>
	<!-- END PAGE STYLES -->
@stop


@section('content')

	<!-- BEGIN LOGIN FORM -->
	{!! Form::open(array('action' => 'Canvas\UserController@resetPassword','class'=>'change-form', 'method'=>'post')) !!}
		<div class="form-title">
			<span class="form-title">Change Your Password </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">New Password</label>
			<input type="hidden" name="id" value="{{ $id }}">
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="New Password" name="newPassword" id="newPassword"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm Password" name="confirmPassword" id="confirmPassword"/>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary btn-block uppercase">Update</button>
		</div>
	{!! FORM::close() !!}
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->

@endsection


@section('buttom_scripts')

	<!-- BEGIN PAGE SCRIPTS -->
	<script src="{{ asset('packages/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/admin/pages/login/forgotpassword.js') }}" type="text/javascript"></script>

	<script>
	jQuery(document).ready(function() {    
	   Login.init(); 
	});
	</script>

	<!-- END PAGE SCRIPTS -->

@stop