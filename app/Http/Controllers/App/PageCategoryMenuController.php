<?php
namespace App\Http\Controllers\App;

use App\Models\ProductCategoryLevel;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageCategoryMenuController extends Controller {


	public function showall(){
		$categoryMenus = ProductCategoryLevel::with('ProductCategory')->orderBy('position')->get();

		return Response::json($categoryMenus);
	}

}
