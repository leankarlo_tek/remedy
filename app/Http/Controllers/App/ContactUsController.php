<?php
namespace App\Http\Controllers\App;

use App\Models\Contact;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller {

	
	public function show()
	{
		$view = View::make('admin/pages/contactus', array());
		$view->pageName = 'Contact Us Settings';
		
		return $view;
	}

	public function showall()
	{
		$contacts = Contact::all();
		return Response::json($contacts);
	}


	public function showContact($id){
		$contact = Contact::find($id);
		return Response::json($contact);
	}

}