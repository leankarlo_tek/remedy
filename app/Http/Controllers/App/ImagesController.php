<?php
namespace App\Http\Controllers\App;

use App\Models\Image;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ImagesController extends Controller {

	public function showAll()
	{
		$images = Image::all();
		return Response::json($images);
	}
	
}