<?php
namespace App\Http\Controllers\App;

use App\Models\Product;
use App\Models\ProductCategoryDetail;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductCategoryLevel;
use App\Models\ImageColor;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductClientController extends Controller {

	public function getFeaturedProduct()
	{

		$products = Product::with(['image'])
		->where('isPublished', 1)
		->orderBy('updated_at', 'desc')
		->take(9)
		->get();
		return Response::json($products);
	}

	public function getList($category, $page){
		$itemsPerPage = 9;
		$offset = ($page - 1) * $itemsPerPage;

		if($category == 0){
			$products = Product::join('images', 'products.primary_image', '=', 'images.id')
			->where('isPublished','=','1')
			->skip($offset)
			->take($itemsPerPage)
			->get();
		}else{
			$products = Product::join('images', 'products.primary_image', '=', 'images.id')
			->join('product_category_details', 'product_category_details.product_id','=','products.id')
			->where('isPublished','=','1')
			->where('product_category_details.category_id','=',$category)
			->skip($offset)
			->take($itemsPerPage)
			->get();
		}
		if($products->count() == 0){
			return Response::json(array("result" => 'false', 'message' => 'no data found'));
		}
		else{
			return Response::json($products);
		}
	}

	public function getProduct($id){
		$products = Product::with(['image'])
		->with(['productImages.color','productImages.image'])
		->find($id);
		return Response::json($products);
	}

	public function getProductWithColor($id, $color){
		$products = Product::with(['image'])
		->with(['productImages.color','productImages.image'])
		->find($id);
		;
		return Response::json($products);
	}
}