<?php
namespace App\Http\Controllers\App;

use App\Models\ImageColor;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductColorController extends Controller {

	public function getAll(){
		$productImages = ImageColor::all();
		return Response::json($productImages);
	}

	public function update($id){
		// $productImages = ImageColor::find($id)->get()->first();
		// return Response::json($productImages);
	}

	public function create($name)
	{
		try{
			$color = new ImageColor();
			$color->name = $name;
			$color->save();
			return Response::json(array('result'=>'Success', 'message'=>'New color has been added'));
		}
		catch(Exception $e)
		{
			return Response::json(array('result'=>'Failed', 'message'=>$e));
		}

	}
}