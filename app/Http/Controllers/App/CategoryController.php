<?php
namespace App\Http\Controllers\App;

use App\Models\Product;
use App\Models\ProductCategoryDetail;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductCategoryLevel;
use App\Models\ImageColor;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller {

	public function index()
	{
		$view = View::make('admin/products/category_management', array());
		$view->pageName = 'Category';
		return $view;
	}

	public function showAll()
	{
		$categories = ProductCategory::all();
		return $categories;
	}

	public function create($name)
	{
		try{
			$category = new ProductCategory();
			$category->name = $name;
			$category->save();
			return Response::json(array('result'=>'Success', 'message'=>'New Category has been added'));
		}
		catch(Exception $e)
		{
			return Response::json(array('result'=>'Failed', 'message'=>$e));
		}

	}

	public function destroy($id)
	{
		try{
			$category = ProductCategory::find($id);
			$category->delete();
			return Response::json(array('result'=>'Success', 'message'=>'Succefully removed a category'));
		}
		catch(Exception $e)
		{
			return Response::json(array('result'=>'Failed', 'message'=>$e));
		}

	}
	
	
}