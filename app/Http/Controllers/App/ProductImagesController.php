<?php
namespace App\Http\Controllers\App;

use App\Models\Product;
use App\Models\ProductCategoryDetail;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductCategoryLevel;
use App\Models\ImageColor;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductImagesController extends Controller {

	public function index()
	{
		$productImages = ImageColor::all();
		return Response::json($productImages);
	}

	public function getAll(){
		$productImages = ProductImage::with('image','color')->get();
		return Response::json($productImages);
	}

	public function getAllByProductID($id){
		$productImages = ProductImage::with('image','color')
			->where('product_id', '=', $id)
			->get();
		return Response::json($productImages);
	}
}