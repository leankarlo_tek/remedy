<?php
namespace App\Http\Controllers\App;

use App\Models\Contact;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsPageController extends Controller {

	public function getStores()
	{
		$stores = Contact::all();
		return Response::json($stores);
	}
}