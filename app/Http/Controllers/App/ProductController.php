<?php
namespace App\Http\Controllers\App;

use App\Models\Product;
use App\Models\ProductCategoryDetail;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductCategoryLevel;
use App\Models\ImageColor;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller {

	public function Product_getAll()
	{
		$products = Product::with(['productCategoryDetails.productCategory'])
		->get();
		return Response::json($products);
	}

	public function showlist($id)
	{
		$variable = Product::get_products($id);
		// dd('test');
		echo "<pre>";
		foreach ($variable as $key => $value) {
			dd($key);
		}
	}

	public function productCategoryShow($id)
	{
		$productCategories = Product::with('productCategoryDetails.productCategory')
		->where('id' , '=' , $id)
		->get()->first();
		return Response::json($productCategories);
	}

}