<?php
namespace App\Http\Controllers\App;

use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Page Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    |
    */

    public function index()
	{
		$view               = View::make('app.home.home', array());
        return $view;
    }

    public function about()
    {
        $view               = View::make('app.about.about', array());
        return $view;
    }

    public function servicePage()
    {
        $view               = View::make('app.service.index', array());
        return $view;
    }

    public function videoPage()
    {
        $view               = View::make('app.videoprod.index', array());
        return $view;
    }

    public function eventPage()
    {
        $view               = View::make('app.event.index', array());
        return $view;
    }

    public function newsPage()
    {
        $view               = View::make('app.news.index', array());
        return $view;
    }

    public function contact()
    {
        $view               = View::make('app.contact.index', array());
        return $view;
    }

    public function sendEmailInquiry(Request $request)
    {
        $name = $request->input('contacts-name');
        $email = $request->input('contacts-email');
        $business = $request->input('contacts-business');
        $messages = $request->input('contacts-message');

        Mail::send('emails.inquiry', ['name' => $name, 'messages' => $messages, 'business' => $business, 'email' => $email ], function ($message) use ($email)
        {

            $message->subject('Inquiry');
            $message->from($email);
            $message->to('sites@saduwa.com');
            // $message->to('lean@teknolohiya.ph');

        });

        // return response()->json(['message' => 'Request completed']);

        $view               = View::make('app.home.home', array());
        return $view;

    }

}
