<?php

namespace App\Http\Controllers\Canvas;

use App\Models\Document;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Images Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    |
    */


    /**
     * Get all the modules that is allowed to the current user
     *
     * @param  array  $data
     * @return Modules
     */
    protected function uploadDocuments(Request $request)
    {
        $file = $request->file;
        $pubpath = public_path();
        $destinationPath = $pubpath.'/uploads/documents';
        $fileOriginalName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        //$filename = $file->getClientOriginalName();
        $filename = basename($fileOriginalName, '.' . $fileExtension) . '.' . $fileExtension;
        $msg = 'Successful';
        $fileNotExists = false;
        while ($fileNotExists == false) {
            $fileNotExists = Document::where('filename', '=', $filename)->get()->isEmpty();
            if ($fileNotExists == false) {
                $filename = basename($filename, '.' . $fileExtension) . '_copy' . '.' . $fileExtension;
                $msg = 'Duplicate file found. The file has been renamed to ' . $filename;
            } 
        }

        $json['files'][] = array(
        'name' => $filename,
        'size' => $file->getSize(),
        //'type' => $file->getMimeType(),
        'url' => '/uploads/documents/'.$filename,
        'deleteType' => 'DELETE',
        );

        $upload = $file->move( $destinationPath, $filename);

        $docs = new Document;
        $docs->filename   = $filename;
        $docs->url        = '/uploads/documents/' . $filename;
        $docsResult = $docs->save();
        
        if ($docsResult) {
            return Response::json(array('message' => $msg, 'filename' => $filename), 200);
        } else {
            return Response::json('error', 400);
        }

    }

    protected function showAll()
    {
        return array('data'=>Document::all());
    }

    protected function deleteDocument($id)
    {
        try
        {
            $doc = Document::find($id);
            
            $path = $_SERVER['DOCUMENT_ROOT'].'/uploads/documents/'. $doc->filename;
            unlink($path);
            
            $docName = $doc->filename;
            $doc->delete();
            return Response::json(array('result' => 'Success', 'message' => $docName . ' has been deleted.'));
        }
        catch(Exception $e)
        {
            return Response::json(array('result' => 'Failed', 'message' => 'Failed to delete '. $docName . '/n Error: ' . $e));
        }
        
    }   
}
