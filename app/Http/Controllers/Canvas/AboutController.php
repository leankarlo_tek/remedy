<?php

namespace App\Http\Controllers\Canvas;

use App\Models\About;
// use App\Models\ArticleType;
// use App\Models\ArticleTag;
// use App\Models\ArticleTypeLevel;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Article Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    |
    */

    //show functions
    protected function headerTitle_update(Request $request){
        
        // INITIALIZATION
        $input = $request->all();

        $about = About::find(1);
        $about->content = $input['title']
        $about->save()

        return Response::json(array('result'=>'true', 'message'=>'Successfully Updated!!'));
    }


    protected function headerDescription_update(Request $request){
        
        // INITIALIZATION
        $input = $request->all();

        $about = About::find(2);
        $about->content = $input['description']
        $about->save()

        return Response::json(array('result'=>'true', 'message'=>'Successfully Updated!!'));
    }

    protected function headerBackground_update(Request $request){
        
        // INITIALIZATION
        $input = $request->all();

        $about = About::find(3);
        $about->content = $input['image']
        $about->save()

        return Response::json(array('result'=>'true', 'message'=>'Successfully Updated!!'));
    }

}
