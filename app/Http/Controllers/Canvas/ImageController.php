<?php

namespace App\Http\Controllers\Canvas;

use App\Models\Image;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Images Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    |
    */


    /**
     * Get all the modules that is allowed to the current user
     *
     * @param  array  $data
     * @return Modules
     */
    protected function uploadImages(Request $request)
    {
        $file = $request->file;
        $pubpath = public_path();
        $destinationPath = $pubpath.'/uploads/images';
        $fileOriginalName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        //$filename = $file->getClientOriginalName();
        $filename = basename($fileOriginalName, '.' . $fileExtension) . '.' . $fileExtension;
        $msg = 'Successful';
        $fileNotExists = false;
        while ($fileNotExists == false) {
            $fileNotExists = Image::where('filename', '=', $filename)->get()->isEmpty();
            if ($fileNotExists == false) {
                $filename = basename($filename, '.' . $fileExtension) . '_copy' . '.' . $fileExtension;
                $msg = 'Duplicate file found. The file has been renamed to ' . $filename;
            } 
        }

        $json['files'][] = array(
        'name' => $filename,
        'size' => $file->getSize(),
        //'type' => $file->getMimeType(),
        'url' => '/uploads/images/'.$filename,
        'deleteType' => 'DELETE',
        );

        $upload = $file->move( $destinationPath, $filename);

        $images = new Image;
        $images->filename   = $filename;
        $images->url        = '/uploads/images/' . $filename;
        $imageResult = $images->save();
        
        if ($imageResult) {
            return Response::json(array('message' => $msg, 'filename' => $filename), 200);
        } else {
            return Response::json('error', 400);
        }

    }

    protected function showAll()
    {
        return Response::json(array('data'=>Image::all()));
    }

    protected function deleteImage($id)
    {
        try
        {
            $image = Image::find($id);
            
            $path = $_SERVER['DOCUMENT_ROOT'].'/uploads/images/'. $image->filename;
            unlink($path);

            $imageName = $image->filename;
            $image->delete();

            return Response::json(array('result' => 'Success', 'message' => $imageName . ' has been deleted.'));
        }
        catch(Exception $e)
        {
            return Response::json(array('result' => 'Failed', 'message' => 'Failed to delete '. $imageName . '/n Error: ' . $e));
        }
        
    }

}
