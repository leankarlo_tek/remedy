<?php

namespace App\Http\Controllers\Canvas;

use App\Models\Music;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MusicController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Images Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    |
    */


    /**
     * Get all the modules that is allowed to the current user
     *
     * @param  array  $data
     * @return Modules
     */
    protected function uploadMusics(Request $request)
    {
        $file = $request->file;
        $pubpath = public_path();
        $destinationPath = $pubpath.'/uploads/musics';
        $fileOriginalName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        //$filename = $file->getClientOriginalName();
        $filename = basename($fileOriginalName, '.' . $fileExtension) . '.' . $fileExtension;
        $msg = 'Successful';
        $fileNotExists = false;
        while ($fileNotExists == false) {
            $fileNotExists = Music::where('filename', '=', $filename)->get()->isEmpty();
            if ($fileNotExists == false) {
                $filename = basename($filename, '.' . $fileExtension) . '_copy' . '.' . $fileExtension;
                $msg = 'Duplicate file found. The file has been renamed to ' . $filename;
            } 
        }

        $json['files'][] = array(
        'name' => $filename,
        'size' => $file->getSize(),
        //'type' => $file->getMimeType(),
        'url' => '/uploads/musics/'.$filename,
        'deleteType' => 'DELETE',
        );

        $upload = $file->move( $destinationPath, $filename );

        $musics = new Music;
        $musics->filename   = $filename;
        $musics->url        = '/uploads/musics/' . $filename;
        $musicsResult = $musics->save();
        
        if ($musicsResult) {
            return Response::json(array('message' => $msg, 'filename' => $filename), 200);
        } else {
            return Response::json('error', 400);
        }

    }

    protected function showAll()
    {
        return array('data'=>Music::all());
    }

    protected function deleteMusic($id)
    {
        try
        {
            $musics = Music::find($id);
            
            $path = $_SERVER['DOCUMENT_ROOT'].'/uploads/musics/'. $musics->filename;
            unlink($path);

            $musicName = $musics->filename;
            $musics->delete();
            return Response::json(array('result' => 'Success', 'message' => $musicName . ' has been deleted.'));
        }
        catch(Exception $e)
        {
            return Response::json(array('result' => 'Failed', 'message' => 'Failed to delete '. $musicName . '/n Error: ' . $e));
        }
        
    }   
}
