<?php

/*
|--------------------------------------------------------------------------
| CANVAS Routes
|--------------------------------------------------------------------------
|
| 1. Do not touch main canvas routes
| 2. You can add routes for your module but do not touch Main Routes
| 3. Create seperate repo if you want to edit main canvas routes
|	Main Canvas Routes :
|	- dashboard
|	- login
|	- users
|
*/

/* MAIN ROUTES */
/* CANVAS ROUTES */
	// login
	Route::get('/login', function () {
	    return view('canvas/login/index');
	});

	// Authenticate User
	Route::post('auth/login', 'Auth\AuthController@create');

	Route::get('auth/logout', 'Auth\AuthController@destroy');
	
	/* END MAIN ROUTES */
	
	//***************************************************

	/* MODULES ROUTES*/
	Route::group(array('middleware' => 'auth','prefix'=>'canvas'), function(){
		
		/** Main Canvas **/
			Route::get('/', function () {
			    return view('canvas/dashboard/index');
			});
			// get modules
			
			Route::group(array('prefix'=>'module'), function(){
				// only display module that is allowed to the current user
				Route::get('/getAllUserModule', 'Canvas\ModuleController@getUserModules');

				Route::get('/getAll', 'Canvas\ModuleController@getAllModules');
				
				Route::get('/getAllActive', 'Canvas\ModuleController@getAllActiveModules');
			});

			/*
			Route::get('submodule/get&module_id={id}', 'Canvas\SubModuleController@getUserAccessSubModules');
			*/

		/** END Main Canvas **/

		/** User Module **/
			Route::group(array('prefix'=>'users'), function(){
				Route::get('/create', function () {
			    	return view('canvas/users/create');
				});

				Route::get('/management', function () {
			    	return view('canvas/users/manage');
				});

				Route::get('/edit&id={id}', function () {
					return view('canvas/users/edit');
				});

				Route::get('/get&id={id}',
				array('uses'=>'Canvas\UserController@getUser'));

				Route::post('/update',
				array('uses'=>'Canvas\UserController@update'));

				Route::post('/changepassword',
				array('uses'=>'Auth\PasswordController@update'));

				Route::post('/insert', 'Canvas\UserController@create');

				Route::get('/show', 'Canvas\UserController@showAll');

				Route::get('/delete&id={id}',
				array('uses'=>'Canvas\UserController@delete'));

				Route::get('/deactivate&id={id}',
				array('uses'=>'Canvas\UserController@deActivate'));

				Route::get('/activate&id={id}',
				array('uses'=>'Canvas\UserController@activate'));

				Route::post('/validate/email',
				array('uses'=>'Canvas\UserController@validateEmail'));

				Route::post('/validate/password', 'Auth\PasswordController@validatePassword');
				
					/** Access Type **/
					Route::group(array('prefix'=>'accesstype'), function(){
	
						Route::get('/getall','Canvas\UserController@getAllAccessTypes');
					
						Route::get('/create_new&name={name}','Canvas\UserController@CreateAccessType');
					
						Route::get('/get&id={id}',
						array('uses'=>'Canvas\UserController@getAccessType'));

						Route::get('/delete&id={id}','Canvas\UserController@deleteAccessType');
					
						Route::get('/getuseraccessmodules&id={id}','Canvas\UserController@getAllModulesByAccessType');

						// Route::post('/update', 'Canvas\UserController@updateAccessType');

						Route::get('/update&id={id}&name={name}','Canvas\UserController@updateAccessType');

						Route::get('/deleteAccessModuleByAccessType&id={id}','Canvas\UserController@deleteAccessModuleByAccessID');

						Route::get('/createAccessModule&accessid={accessid}&moduleid={moduleid}','Canvas\UserController@createAccessModule');
					});
					
				/** END Product Category **/
				
			});
		/** END User Module **/

		/** Media Module **/
			Route::group(array('prefix'=>'media'), function(){         
				  
                /** Image Module **/
                    Route::group(array('prefix'=>'images'), function(){

						Route::get('/manage', function () {
					    	return view('canvas/images/manage');
						});

						Route::get('/upload', function () {
					    	return view('canvas/images/upload');
						});

						Route::post('/uploadimage', 
						array('uses'=>'Canvas\ImageController@uploadImages'));
						
						Route::get('/show', 'Canvas\ImageController@showAll');

						Route::get('/delete&id={id}',
						array('uses'=>'Canvas\ImageController@deleteImage'));    
						                    
                    });
                /** END Image Module **/

                /** Music Module **/
                    Route::group(array('prefix'=>'musics'), function(){
                        
						Route::get('/upload', function () {
					    	return view('canvas/musics/upload');
					    });  

						Route::get('/manage', function () {
					    	return view('canvas/musics/manage');
						});  

						Route::post('/uploadmusic', 
						array('uses'=>'Canvas\MusicController@uploadMusics'));

						Route::get('/show', 'Canvas\MusicController@showAll'); 

						Route::get('/delete&id={id}',
						array('uses'=>'Canvas\MusicController@deleteMusic'));  
                    });
                /** END Music Module **/

                /** Document Module **/
                    Route::group(array('prefix'=>'documents'), function(){
						
						Route::get('/upload', function () {
					    	return view('canvas/documents/upload');
					    });

						Route::get('/manage', function () {
					    	return view('canvas/documents/manage');
					    });  
                        
						Route::post('/uploaddocument', 
						array('uses'=>'Canvas\DocumentController@uploadDocuments'));

						Route::get('/show', 'Canvas\DocumentController@showAll');

						Route::get('/delete&id={id}',
						array('uses'=>'Canvas\DocumentController@deleteDocument'));  
        
                    });

                    
                    
                /** END Files Module **/			

			});
		/** END Media Module **/

		/** Article Module **/
			Route::group(array('prefix'=>'articles'), function(){
				
				Route::get('/', function () {
			    	return view('canvas/articles/myarticles');
				});

				Route::get('/manage', function () {
			    	return view('canvas/articles/manage');
				});

				Route::get('/new', function () {
			    	return view('canvas/articles/new');
				});

				Route::get('/show&id={id}', function () {
			    	return view('canvas/articles/show');
				});

				Route::get('/edit&id={id}', function () {
					return view('canvas/articles/edit');
				});

				Route::get('/manage', function () {
					return view('canvas/articles/manage');
				});
	
				Route::post('/create', 'Canvas\ArticleController@create');

				Route::post('/update', 'Canvas\ArticleController@update');

				Route::get('/delete&id={id}',
				array('uses'=>'Canvas\ArticleController@delete'));

				Route::get('/unpublish&id={id}',
				array('uses'=>'Canvas\ArticleController@deActivate'));

				Route::get('/publish&id={id}',
				array('uses'=>'Canvas\ArticleController@activate'));

				Route::get('/types/showall', 'Canvas\ArticleController@getArticleTypes');

				Route::get('/myarticles','Canvas\ArticleController@getMyArticles');

				Route::get('/getall','Canvas\ArticleController@getAllArticles');

				Route::get('/get&id={id}',
				array('uses'=>'Canvas\ArticleController@getArticle'));

				/** Article Category **/
					Route::group(array('prefix'=>'category'), function(){
	
						
						Route::get('/level/getall','Canvas\ArticleController@GetArticleTypeLevel');

						Route::get('/getall','Canvas\ArticleController@LoadCategorySelection');
	
						Route::get('/create_new&name={name}','Canvas\ArticleController@CreateNewCategory');
	
						Route::post('/level/update', 'Canvas\ArticleController@UpdateLevel');

						Route::get('/level/add&id={id}', 'Canvas\ArticleController@AddCategoryLevel');

						Route::get('/level/delete&id={id}', 'Canvas\ArticleController@DeleteCategoryLevel');
					
					});
				/** END Product Category **/

			});
		/** END Article Module **/

		/** Project Module **/
			Route::group(array('prefix'=>'projects'), function(){
				Route::get('/manage', function () {
			    	return view('canvas/projects/manage');
				});

				Route::get('/new', function () {
			    	return view('canvas/projects/new');
				});

				Route::get('/show&id={id}', function () {
			    	return view('canvas/projects/show');
				});

				Route::get('/edit&id={id}', function () {
					return view('canvas/projects/edit');
				});
	
				Route::post('/create', 'Canvas\ProjectController@create');

				Route::post('/update', 'Canvas\ProjectController@update');

				Route::get('/delete&id={id}',
				array('uses'=>'Canvas\ProjectController@delete'));

				Route::get('/unpublish&id={id}',
				array('uses'=>'Canvas\ProjectController@deActivate'));

				Route::get('/publish&id={id}',
				array('uses'=>'Canvas\ProjectController@activate'));

				Route::get('/types/showall', 'Canvas\ProjectController@getProjectTypes');

				Route::get('/getAllProjects','Canvas\ProjectController@getAllProjects');

				Route::get('/get&id={id}','Canvas\ProjectController@getProject');


				/** Project Category **/
					Route::group(array('prefix'=>'category'), function(){
	
						Route::get('/level/getall','Canvas\ProjectController@GetProjectTypeLevel');

						Route::get('/getall','Canvas\ProjectController@LoadCategorySelection');
	
						Route::get('/create_new&name={name}','Canvas\ProjectController@CreateNewCategory');
	
						Route::post('/level/update', 'Canvas\ProjectController@UpdateLevel');

						Route::get('/level/add&id={id}', 'Canvas\ProjectController@AddCategoryLevel');

						Route::get('/level/delete&id={id}', 'Canvas\ProjectController@DeleteCategoryLevel');
					
					});
				/** END Product Category **/

			});
		/** END Project Module **/

		/** Pages Module **/
			Route::group(array('prefix'=>'pages'), function(){

				/** main **/
					Route::group(array('prefix'=>'main'), function(){
					
						Route::get('/', function () {
							return view('canvas/pages/mainpage');
						});
						
					});
				/** END About **/
				

				/** About **/
					Route::group(array('prefix'=>'about'), function(){
					
						Route::get('/', function () {
							return view('canvas/pages/about');
						});
						
					});
				/** END About **/

				/** Promos **/
					Route::group(array('prefix'=>'promos'), function(){
					
						Route::get('/', function () {
							return view('canvas/pages/promomanagement');
						});
	
						Route::get('/getall','Canvas\PromosController@getAll');
	
						Route::post('/insert', 'Canvas\PromosController@create');
	
						Route::get('/delete&id={id}',
						array('uses'=>'Canvas\PromosController@delete'));
		
						Route::get('/unpublish&id={id}',
						array('uses'=>'Canvas\PromosController@deActivate'));
		
						Route::get('/publish&id={id}',
						array('uses'=>'Canvas\PromosController@activate'));
						
					});
				/** END Promos **/

				/** Slider **/
					Route::group(array('prefix'=>'slider'), function(){
					
						Route::get('/', function () {
							return view('canvas/pages/slider');
						});
	
						Route::get('/getall','Canvas\SliderController@getAll');
	
						Route::post('/insert', 'Canvas\SliderController@create');
	
						Route::get('/delete&id={id}',
						array('uses'=>'Canvas\SliderController@delete'));
		
						Route::get('/unpublish&id={id}',
						array('uses'=>'Canvas\SliderController@deActivate'));
		
						Route::get('/publish&id={id}',
						array('uses'=>'Canvas\SliderController@activate'));
						
					});
				/** END Slider **/

				/** Contact **/
					Route::group(array('prefix'=>'contact'), function(){
					
						Route::get('/', function () {
							return view('canvas/pages/contact');
						});
	
						Route::get('/getall','Canvas\ContactController@getAll');
	
						Route::get('/get&id={id}',
						array('uses'=>'Canvas\ContactController@getContact'));
	
						Route::post('/insert', 'Canvas\ContactController@create');
	
						Route::post('/update', 'Canvas\ContactController@update');
	
						Route::get('/delete&id={id}',
						array('uses'=>'Canvas\ContactController@delete'));
		
						Route::get('/unpublish&id={id}',
						array('uses'=>'Canvas\ContactController@deActivate'));
		
						Route::get('/publish&id={id}',
						array('uses'=>'Canvas\ContactController@activate'));
						
					});
				/** END Promos **/
				
			});
		/** END Pages Module **/

		/** Product Module **/
			Route::group(array('prefix'=>'products'), function(){
				
				Route::get('/manage', function () {
			    	return view('canvas/products/manage');
				});

				Route::get('/categorymanage', function () {
			    	return view('canvas/products/categorymanage');
				});

				Route::get('/getall','Canvas\ProductController@ShowAll');

				Route::post('/create', 'Canvas\ProductController@Create');

				Route::post('/update', 'Canvas\ProductController@Update');

				Route::get('/get&id={id}','Canvas\ProductController@GetProduct');

				Route::get('/delete&id={id}','Canvas\ProductController@DeleteProduct');

				Route::get('/publish&id={id}','Canvas\ProductController@PublishProduct');

				Route::get('/unpublish&id={id}','Canvas\ProductController@UnPublishProduct');

				/** Product Category **/
					Route::group(array('prefix'=>'category'), function(){
	
						Route::get('/get&id={id}','Canvas\ProductController@GetProductCategory');
						
						Route::get('/level/getall','Canvas\ProductController@GetProductCategoryLevel');

						Route::get('/getall','Canvas\ProductController@LoadCategorySelection');
	
						Route::get('/delete&id={id}','Canvas\ProductController@DeleteProductCategory');
	
						Route::get('/add&id={id}&product_id={product_id}','Canvas\ProductController@AddProductCategory');
	
						Route::get('/create_new&name={name}','Canvas\ProductController@CreateNewCategory');
	
						Route::post('/level/update', 'Canvas\ProductController@UpdateLevel');

						Route::get('/level/add&id={id}', 'Canvas\ProductController@AddCategoryLevel');

						Route::get('/level/delete&id={id}', 'Canvas\ProductController@DeleteCategoryLevel');
					
					});
				/** END Product Category **/

				/** Product Image **/
					Route::group(array('prefix' => 'image'), function(){
	
						Route::get('/get&id={id}', 'Canvas\ProductController@GetProductImage');

						Route::get('/color/getall','Canvas\ProductController@LoadColorSelection');

						Route::get('/delete&id={id}','Canvas\ProductController@DeleteProductImage');

						Route::get('/color/update&id={id}&color_id={color_id}','Canvas\ProductController@SaveProductImageColor');

						Route::get('/create&id={id}&product_id={product_id}','Canvas\ProductController@SaveProductImage');
	
					});
				/** END Product Image **/

			});
		/** END Product Module **/

	});
	/* END MODULES ROUTES */

/* CANVAS ROUTES END */
