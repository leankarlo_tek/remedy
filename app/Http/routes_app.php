<?php

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
|
|
*/

/** Web App Route **/

Route::get('/', 'HomeController@index');

Route::get('/about'		, 'HomeController@about');
Route::get('/service'	, 'HomeController@servicePage');
Route::get('/video'		, 'HomeController@videoPage');
Route::get('/event'		, 'HomeController@eventPage');
Route::get('/news'		, 'HomeController@newsPage');
Route::get('/contact'	, 'HomeController@contact');

Route::post('/inquiry',
  ['as' => 'contact_us', 'uses' => 'HomeController@sendEmailInquiry']);
	
/** END Web App Route **/	